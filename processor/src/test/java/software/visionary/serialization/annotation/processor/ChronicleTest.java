package software.visionary.serialization.annotation.processor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.annotation.processing.Filer;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.*;

class ChronicleTest {
    @Mock
    TypeElement type;
    @Mock
    PackageElement pack;
    Chronicle toTest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        toTest = new Chronicle(type);
        final Name qual = mock(Name.class);
        given(type.getQualifiedName()).willReturn(qual);
        final Name simple = mock(Name.class);
        given(type.getSimpleName()).willReturn(simple);
        given(type.getEnclosingElement()).willReturn(pack);
        given(pack.getKind()).willReturn(ElementKind.PACKAGE);
        final Name packageName = mock(Name.class);
        given(pack.getQualifiedName()).willReturn(packageName);
        given(packageName.chars()).willReturn("foo.bar".chars());
    }

    @Test
    void canEmitTemplate() {
        final String result = toTest.templated();
        assertNotNull(result);
        verify(type).getQualifiedName();
        verify(type).getSimpleName();
    }

    @Test
    void doesNotWriteSameTwice() throws IOException {
        final Filer f = mock(Filer.class);
        final ProcessingEnvironment p = mock(ProcessingEnvironment.class);
        final Messager m = mock(Messager.class);
        given(p.getFiler()).willReturn(f);
        given(p.getMessager()).willReturn(m);
        given(f.createSourceFile(anyString(), any())).willThrow(new IOException());
        toTest.writeIfNecessary(p);
        toTest.writeIfNecessary(p);
        verify(p, atMostOnce()).getFiler();
        verify(p).getMessager();
        verify(m).printMessage(eq(Diagnostic.Kind.ERROR), anyString());
    }

    @Test
    void canWriteToOutputStream() throws IOException {
        final Filer f = mock(Filer.class);
        final ProcessingEnvironment p = mock(ProcessingEnvironment.class);
        final Messager m = mock(Messager.class);
        final JavaFileObject jo = mock(JavaFileObject.class);
        given(p.getFiler()).willReturn(f);
        given(p.getMessager()).willReturn(m);
        given(f.createSourceFile(anyString(), any())).willReturn(jo);
        final OutputStream os = mock(OutputStream.class);
        given(jo.openOutputStream()).willReturn(os);
        toTest.writeIfNecessary(p);
        verify(p).getFiler();
        verify(f).createSourceFile(anyString(), any());
        verify(jo).openOutputStream();
        verify(os).write(any(byte[].class));
    }
}
