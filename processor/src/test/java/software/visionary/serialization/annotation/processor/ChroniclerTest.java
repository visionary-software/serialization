package software.visionary.serialization.annotation.processor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import software.visionary.serialization.annotation.api.Serializable;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

class ChroniclerTest {
    @Mock
    RoundEnvironment round;
    Chronicler toTest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        toTest = new Chronicler();
    }


    @Test
    void supportsOnlySerializable() {
        final Set<String> supported = toTest.getSupportedAnnotationTypes();
        assertTrue(supported.contains(Serializable.class.getCanonicalName()));
    }

    @Test
    void supportsLatestVersion() {
        final SourceVersion supported = toTest.getSupportedSourceVersion();
        assertEquals(SourceVersion.latestSupported(), supported);
    }

    @Test
    void doesNothingIfProcessingIsOver() {
        given(round.processingOver()).willReturn(true);
        assertFalse(toTest.process(Set.of(), round));
        verify(round).processingOver();
        verifyNoMoreInteractions(round);
    }

    @Test
    void canGetElementsAnnotatedWithSerializable() {
        final Messager m = mock(Messager.class);
        final ProcessingEnvironment pro = mock(ProcessingEnvironment.class);
        given(pro.getMessager()).willReturn(m);
        toTest.init(pro);
        final TypeElement elem = mock(TypeElement.class);
        given(round.getElementsAnnotatedWith(Serializable.class)).willAnswer(invoc -> Set.of(elem));
        assertFalse(toTest.process(Set.of(), round));
        verify(round).getElementsAnnotatedWith(Serializable.class);
        verify(elem, atLeastOnce()).getQualifiedName();
    }
}
