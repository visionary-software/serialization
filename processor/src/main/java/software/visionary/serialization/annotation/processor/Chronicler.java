/*
 *
 * Copyright (c) 2023-2025.
 * Nico Vaidyanathan Hidalgo
 * Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions SERIALIZATION.
 *
 * Visionary Software Solutions SERIALIZATION is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Visionary Software Solutions SERIALIZATION is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions SERIALIZATION.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.serialization.annotation.processor;

import software.visionary.serialization.annotation.api.Serializable;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * An {@link AbstractProcessor} that discovers all {@link Serializable} and generates an
 * {@link software.visionary.serialization.annotation.api.SerializationStrategy} for them based on a template.
 */
public final class Chronicler extends AbstractProcessor {
    /**
     * @return only {@link Serializable} is supported.
     */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Set.of(Serializable.class.getCanonicalName());
    }

    /**
     * @return in principle this could be back-ported and made to work in Java versions like 8 or before by
     * replacing Records and convenient Stream-based APIs. Since this whole approach is an experiment in working in
     * modern Java, the latest version (17 when written) is fine.
     */
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    /**
     * Finds all the {@link Serializable} in the {@link RoundEnvironment} and generates a
     * {@link software.visionary.serialization.annotation.api.SerializationStrategy} for them if it has not
     * already been generated.
     * @param set unused. I think this is a bug in the Annotation Processor API as I haven't seen one good example
     *            that uses this for anything. Even Google's AutoProcessor just discards it. :|
     * @param roundEnvironment processed
     * @return always false to allow other annotation processors to do work on Serializable.
     * Potentially annotation processors could be used to generate SerializationStrategies in various formats.
     */
    @Override
    public boolean process(final Set<? extends TypeElement> set, final RoundEnvironment roundEnvironment) {
        if (roundEnvironment.processingOver()) {
            return false;
        }

        roundEnvironment.getElementsAnnotatedWith(Serializable.class)
                .stream()
                .filter(TypeElement.class::isInstance)
                .map(TypeElement.class::cast)
                .map(Chronicle::new)
                .forEach(c -> c.writeIfNecessary(processingEnv));
        return false;
    }
}
