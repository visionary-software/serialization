/*
 *
 * Copyright (c) 2025.
 * Nico Vaidyanathan Hidalgo
 * Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions SERIALIZATION.
 *
 * Visionary Software Solutions SERIALIZATION is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Visionary Software Solutions SERIALIZATION is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions SERIALIZATION.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.serialization.annotation.processor;
import software.visionary.mustached.Mustached;

/**
 * {@link Mustached} for great justice.
 * @param pack the package name of the class
 * @param canonical the canonical class name
 * @param simple the simple class name
 */
public record SerializationStrategyTemplate(String pack, String canonical, String simple) implements Mustached {
	@Override
	public String toString() { return string(); }
}
