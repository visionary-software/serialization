/*
 *
 * Copyright (c) 2023-2025.
 * Nico Vaidyanathan Hidalgo
 * Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions SERIALIZATION.
 *
 * Visionary Software Solutions SERIALIZATION is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Visionary Software Solutions SERIALIZATION is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions SERIALIZATION.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.serialization.annotation.processor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Generates {@link software.visionary.serialization.annotation.api.SerializationStrategy} specified interfaces for
 * {@link software.visionary.serialization.annotation.api.Serializable} classes by applying the
 * <a href="https://mustache.github.io/" target="_blank">Mustache</a> template in resources/SerializationStrategy.template.
 */
final class Chronicle {
    /**
     * Since AnnotationProcessing happens in multiple rounds, it is important to keep track of which classes
     * have already been processed.
     */
    private static final List<String> WRITTEN = new ArrayList<>();
    /**
     * A {@link Class} marked with {@link software.visionary.serialization.annotation.api.Serializable} discovered.
     */
    private final TypeElement element;

    /**
     * @param e must not be null.
     */
    Chronicle(final TypeElement e) {
        element = Objects.requireNonNull(e);
    }

    /**
     * @return provides the complete ancestor tree of the TypeElement to potentially do interesting things
     * based on if it is an inner class, in a module, etc.
     */
    Element[] elements() {
        return Stream.of(element).takeWhile(e -> e.getEnclosingElement() != null).toArray(Element[]::new);
    }

    /**
     * Applies the Mustache template.
     *
     * For instance:
     * <pre><code>
     * \@Serializable class Philosopher{}
     * </code></pre>
     *
     * will result in:
     * <pre><code>
     * \@Generated public interface PhilosopherSerializationStrategy extends SerializationStrategy\<Philosopher>* </code></pre>
     * @return the string containing the template
     */
    String templated() {
        return new SerializationStrategyTemplate(
                getPackage().getQualifiedName().toString(),
                element.getQualifiedName().toString(),
                element.getSimpleName().toString()
                ).toString();
    }

    private PackageElement getPackage() {
        Element enclosing = element;
        while (enclosing.getKind() != ElementKind.PACKAGE) {
            enclosing = enclosing.getEnclosingElement();
        }
        return (PackageElement) enclosing;
    }

    /**
     * @return output file in the form of XSerializationStrategy.
     */
    String location() {
        return element.getQualifiedName() + "SerializationStrategy";
    }

    /**
     * writes the template only if it has not been previously written, as this would result in an
     * error due to {@link javax.annotation.processing.Filer}.
     * @param p contains the Filer and Messager
     */
    void writeIfNecessary(final ProcessingEnvironment p) {
        if(!WRITTEN.contains(location())) {
            writeIt(p);
            WRITTEN.add(location());
        }
    }

    /**
     * creates a Java Source file--which may be processed by subsequent annotation processing rounds-- and
     * writes it to the specified location.
     *
     * @param p the processing environment
     */
    void writeIt(final ProcessingEnvironment p) {
        try (final OutputStream o = p.getFiler().createSourceFile(location(), elements()).openOutputStream()) {
            o.write(templated().getBytes(StandardCharsets.UTF_8));
        } catch (final Exception e) {
            p.getMessager().printMessage(Diagnostic.Kind.ERROR, e.toString());
        }
    }
}
