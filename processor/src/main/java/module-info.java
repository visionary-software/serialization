/**
 * An optional Annotation Processing component that is useful for integrating the Serialization API with
 * Bugger, or any SERVICE REGISTRY or DEPENDENCY INJECTION framework that tries to be type-safe.
 *
 * When using something like {@link java.util.ServiceLoader} only registering a
 * {@link software.visionary.serialization.annotation.api.SerializationStrategy} results in
 * potentially multiple implementations being registered for different classes and runtime type checking
 * to determine if a particular implementation can serialize a given object.
 *
 * Using Gafter's Gadget to store TypeReferences in the Guice TypeLiteral/Key style might work better for parametrized
 * types, but also requires a certain bit of arcana in the implementation of a framework that complicates life.
 * It's simpler to register and lookup XSerializationStrategy that resolves the parameterized type, but this creates
 * a burden on users with little interfaces that are simple to mechanically generate.
 */
module software.visionary.serialization.annotation.processor {
    requires java.compiler;
    requires software.visionary.serialization.annotation.api;
	requires software.visionary.mustached;
}