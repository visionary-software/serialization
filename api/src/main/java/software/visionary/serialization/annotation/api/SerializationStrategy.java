/*
 *
 * Copyright (c) 2023-2025.
 * Nico Vaidyanathan Hidalgo
 * Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions SERIALIZATION.
 *
 * Visionary Software Solutions SERIALIZATION is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Visionary Software Solutions SERIALIZATION is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions SERIALIZATION.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.serialization.annotation.api;

/**
 * Converts an object into bytes.
 *
 * No guarantees are made about thread-safety or immutability, though both are considered fine ideas.
 * Implementations may attempt to traverse the entire object graph themselves, or
 * invoke other SerializationStrategy implementations for components.
 *
 * @param <T> the class to serialize into bytes.
 */
public interface SerializationStrategy<T> {
    /**
     * Serialize the object.
     * @param toSerialize to serialize.
     * Null handling is implementation dependent.
     * @return the serialized object. Should not be null.
     */
    byte[] serialize(T toSerialize);

    /**
     * Deserialize the object.
     * @param toDeserialize to deserialize.
     * Null handling is implementation dependent.
     * @return the deserialized object. Should not be null.
     */
    T deserialize(byte[] toDeserialize);
}
