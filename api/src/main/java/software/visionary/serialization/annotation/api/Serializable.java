/*
 *
 * Copyright (c) 2023-2025.
 * Nico Vaidyanathan Hidalgo
 * Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions SERIALIZATION.
 *
 * Visionary Software Solutions SERIALIZATION is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Visionary Software Solutions SERIALIZATION is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions SERIALIZATION.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.serialization.annotation.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Base annotation forming a framework to replace {@link java.io.Serializable}.
 * Josh Bloch's Effective Java pointed out many problems with Serializable, usually dealing with security/safety
 * issues, versioning, and more.
 *
 * This approach doesn't try to be as ambitious as the original serialization spec.
 *
 * Serialization/Deserialization of objects essentially persists their in-memory byte information for storage
 * or transmission. While a compact binary format may be the most desirable in many situations, it's not always
 * the case. As Hunt and Thomas argue in The Pragmatic Programmer, it is often desirable to emit a human-readble
 * representation such as XML/JSON/YAML/etc. One such situation can be sending data through a network when
 * you want to be able to easily debug errors. Although all data in a computer is essentially bits/bytes at the end,
 * including strings, it can be handy to be able to quickly identify the flow of information without having to
 * perform complex byte/hex conversions as the Real Programmers Don't Eat Quiche did in the 1970s.
 *
 * This framework provides a MARKER ANNOTATION instead of a MARKER INTERFACE yet
 * implies a set of heavy constraints as illuminated by Bloch.
 *
 * Implementations of {@link SerializationStrategy} can then be implemented for the class.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Serializable {
}
