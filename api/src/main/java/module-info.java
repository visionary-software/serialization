/**
 * Everything is meant to be exported.
 */
module software.visionary.serialization.annotation.api {
    exports software.visionary.serialization.annotation.api;
}